<?php

class HttpServer
{
	public static $instance;

	public $http;
	public static $get;
	public static $post;
	public static $header;
	public static $server;
	private $application;

	public function __construct() {
		$http = new swoole_http_server("0.0.0.0", 9501);

		$http->set(
			array(
				'worker_num' => 2,
				'daemonize' => 0,
	      'max_request' => 10000,
	      'dispatch_mode' => 1
			)
		);

		$http->on('WorkerStart' , array( $this , 'onWorkerStart'));

		$http->on('request', function ($request, $response) {
			if( isset($request->server) ) {
				foreach ($request->server as $key => $value) {
                    unset($_SERVER[ strtoupper($key) ]);
					$_SERVER[ strtoupper($key) ] = $value;
				}
			}
			if( isset($request->header) ) {
				foreach ($request->header as $key => $value) {
                    unset($_SERVER[ strtoupper($key) ]);
					$_SERVER[ strtoupper($key) ] = $value;
				}
			}
            unset($_GET);
			if( isset($request->get) ) {
				foreach ($request->get as $key => $value) {
					$_GET[ $key ] = $value;
				}
			}
            unset($_POST);
			if( isset($request->post) ) {
				foreach ($request->post as $key => $value) {
					$_POST[ $key ] = $value;
				}
			}
            unset($_COOKIE);
			if( isset($request->cookie) ) {
				foreach ($request->cookie as $key => $value) {
					$_COOKIE[ $key ] = $value;
				}
			}
            unset($_FILES);
			if( isset($request->files) ) {
				foreach ($request->files as $key => $value) {
					$_FILES[ $key ] = $value;
				}
			}
            /*
			$uri = explode( "?", $_SERVER['REQUEST_URI'] );
			$_SERVER["PATH_INFO"] = $uri[0];
			if( isset( $uri[1] ) ) {
				$_SERVER['QUERY_STRING'] = $uri[1];
			}*/
      $_SERVER["PATH_INFO"] = explode('/', $_SERVER["PATH_INFO"],3)[2];
      $_SERVER['argv'][1]=$_SERVER["PATH_INFO"];

			ob_start();
$response->header('Content-Type', 'text/html');
$response->header('charset','utf-8');
    var_dump($_SERVER,$_REQUEST,$_POST,$_GET,$_FILES);

echo "<br/><br/>";

    var_dump($GLOBALS['testvar']);
		  $result = ob_get_contents();
		  ob_end_clean();
		  $response->end($result);
		});

		$http->start();
	}

	public function onWorkerStart() {
    var_dump("start");
    require_once('requirefile1.php');
    var_dump($GLOBALS['testvar']);
	}

	public static function getInstance() {
		if (!self::$instance) {
            self::$instance = new HttpServer;
        }
        return self::$instance;
	}
}

HttpServer::getInstance();
