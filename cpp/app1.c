#include <stdio.h> 

//宏定义
#define MAX(a,b) ((a)>(b)?(a):(b))

//条件定义的
#ifndef TEST
#define TEST
#endif

// 取消宏定义，如果之前没有定义的也不会报错的
#undef ARG1

#define MIN(x,y) x>y?y:x

#define MIN2(A,B)	({ __typeof__(A) __a = (A); __typeof__(B) __b = (B); __a < __b ? __a : __b; })


//占位，原样输出
#define PSQR(x) printf(" The square of " #x " is %d.\n",(( x)*( x)))

//链接，链接成一个符号
#define XNAME(n) x##n

int main(){
	int a = MIN(1,2);
	// => int a = 1 < 2 ? 1 : 2;
	printf("%d\n",a);

	int y = 2;
	PSQR(y);

	int XNAME(1) = 4;
	printf("%d\n", x1);
}