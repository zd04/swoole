<?php
$process = new swoole_process('callback_function', true); 
$pid = $process->start(); 
  
function callback_function(swoole_process $worker) 
{ 
    echo $worker->read();
    //$worker->exec('/usr/bin/php', array(__DIR__."/input.php")); 
} 

$process->write("hello world\n");
echo $pid." => ",PHP_EOL;
echo $process->read();
echo PHP_EOL;
$ret = swoole_process::wait();
var_dump($ret);

//异步的
swoole_process::signal(SIGALRM, function ()
{
    static $i = 0;
    echo "#{$i}\talarm\n";
    $i++;
    if ($i > 20)
    {
        swoole_process::alarm(-1);
    }
});

//定时器
swoole_process::alarm(100 * 1000);

//下面这个定时器会影响上面的定时器,上面的定时器会失效??
swoole_timer_tick(1000, function(){ 
    echo "timeout\n"; 
});