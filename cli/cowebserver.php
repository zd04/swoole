<?php 

$server = new Swoole\Http\Server('127.0.0.1', 9501, SWOOLE_BASE);

$server->set(array('worker_num' => 1));

$server->on('Request', function($request, $response) {

    //协程版的mysql客户端
    $mysql = new Swoole\Coroutine\MySQL();
    $res = $mysql->connect(['host' => '127.0.0.1', 
    'user' => 'root', 
    'password' => 'root', 
    'database' => 'wp']);
    if ($res == false) {
        $response->end("MySQL connect fail!");
        return;
    }
    $ret = $mysql->query('select sleep(1)');
    $response->end("swoole response is ok, result=".var_export($ret, true));
});

$server->start();