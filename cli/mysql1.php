<?php 

$pdo = new PDO("mysql:host=localhost;dbname=test;charset=utf8", 'root', '123456');

class Tab1
{
  public $_data = array();
  public function __construct($d){
    $this->_data = $d;
  }
}

//保存数据的
class Tab2{
  protected $_data = array();

  public function __set($key,$value){
    $this->_data[$key] = $value;
  }
}

$query = "SELECT * FROM tab1";

// PDO
$result = $pdo->query($query);

//第三个参数数构造函数的参数,就是另外增加的参数
// $result->setFetchMode(PDO::FETCH_CLASS, 'Tab1',array('100'));

//自动把结果字段注入到Tab2的实例中
$result->setFetchMode(PDO::FETCH_CLASS, 'Tab2');

while ($tab1One = $result->fetch())
{
    var_dump($tab1One);
}