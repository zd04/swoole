<?php 

function compile($filename) {
	$content = php_strip_whitespace ( $filename );

    //删除开头的<?php 标记
	$content = trim ( substr ( $content, 5 ) );
	// 替换预编译指令
	//$content = preg_replace ( '/\/\/\[RUNTIME\](.*?)\/\/\[\/RUNTIME\]/s', '', $content );

    //如果有命名空间的，保留命名空间
	if (0 === strpos ( $content, 'namespace' )) {
		$content = preg_replace ( '/namespace\s(.*?);/', 'namespace \\1{', $content, 1 );
	} else {
		$content = 'namespace {' . $content;//这个对应全局的命名空间
	}

    //如果结尾有 \?\>标记也删除
	if ('?>' == substr ( $content, - 2 ))
		$content = substr ( $content, 0, - 2 );
    
    //命名空间结尾的
	return $content . '}';
}

$s = compile('phpcompile/indexa.php');

echo $s;