<?php 
//反射

class B{}

class A{
    public function aa(\B $a, Array $b, int $c = 0){
        var_dump($a,$b,$c);
    }

    
}

$f = new \ReflectionMethod('A','aa');
$p = $f->getParameters();
foreach($p as $cp){
    echo "============================",PHP_EOL;//参数的名称
    echo "name ",$cp->getName(),PHP_EOL;
    echo 'isDefaultValueAvailable:',$cp->isDefaultValueAvailable(),PHP_EOL;
    if($cp->isDefaultValueAvailable()){
        echo "defaultValue:",$cp->getDefaultValue(),PHP_EOL;
    }
    echo "position:",$cp->getPosition(),PHP_EOL;
    echo "has type:",$cp->hasType(),PHP_EOL;//参数的类型
    if($cp->getType()){
        echo "type:",$cp->getType(),PHP_EOL;
    }
    
    echo 'is variadic:',$cp->isVariadic(),PHP_EOL;//可变长度参数

    echo PHP_EOL;
}
$obj  = new A();
$bb = new B();
$f->invokeArgs($obj,array($bb,array(1,2,3),1));//注意参数是有顺序的！！！