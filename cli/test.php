<?php 

$redis = new Redis();
$redis->connect('127.0.0.1', 6379, 10); 

//关键是一个一个的处理
$tryTimes = 5;
$listName = "redis_queue";//任务队列的名称

//从执行5次的队列中取出数据
$json = $redis->rPop($listName."_".$tryTimes);

$data = json_decode($json);
$function = $data['function'];
$params = $data['params'];

//处理数据
sleep(3);


//处理结果是成功或者失败
if(rand(0,10) < 7){
  //如果是处理失败的就放到下一次进行处理
  $tryTimes --;
  $redis->lPush($listName.'_'.$tryTimes,$data);
  echo "ERROR";
}else{
  echo "SUCCESS";
}