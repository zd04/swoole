<?php 
$a = [
    'a'=>'1',
    'b'=>['c'=>1]
];

//$var = var_export($a,true);
//var_dump($var);

//导出到php代码的
$s = ' $var = '.var_export($a,true);
echo $s;

//thinkphp编译核心代码到一个文件中

    //编译之后的文件名称 目录/模式~runtime.php,这个是合并之后的文件
      $runtimefile  = RUNTIME_PATH.APP_MODE.'~runtime.php';
      if(!APP_DEBUG && Storage::has($runtimefile)){
          Storage::load($runtimefile);
      }else{
          if(Storage::has($runtimefile))
              Storage::unlink($runtimefile);
          $content =  '';
          // 读取应用模式
          $mode   =   include is_file(CONF_PATH.'core.php')?CONF_PATH.'core.php':MODE_PATH.APP_MODE.'.php';
          // 加载核心文件
          foreach ($mode['core'] as $file){
              if(is_file($file)) {
                include $file;
                if(!APP_DEBUG) $content   .= compile($file);
              }
          }

          // 加载应用模式配置文件
          //加载到一个大的数组中的
          foreach ($mode['config'] as $key=>$file){
              is_numeric($key)?C(load_config($file)):C($key,load_config($file));
          }

          // 读取当前应用模式对应的配置文件
          if('common' != APP_MODE && is_file(CONF_PATH.'config_'.APP_MODE.CONF_EXT))
              C(load_config(CONF_PATH.'config_'.APP_MODE.CONF_EXT));  

          // 加载模式别名定义
          if(isset($mode['alias'])){
              self::addMap(is_array($mode['alias'])?$mode['alias']:include $mode['alias']);
          }

          // 加载应用别名定义文件
          if(is_file(CONF_PATH.'alias.php'))
              self::addMap(include CONF_PATH.'alias.php');

          // 加载模式行为定义
          if(isset($mode['tags'])) {
              Hook::import(is_array($mode['tags'])?$mode['tags']:include $mode['tags']);
          }

          // 加载应用行为定义
          if(is_file(CONF_PATH.'tags.php'))
              // 允许应用增加开发模式配置定义
              Hook::import(include CONF_PATH.'tags.php');   

          // 加载框架底层语言包
          L(include THINK_PATH.'Lang/'.strtolower(C('DEFAULT_LANG')).'.php');

          if(!APP_DEBUG){
              $content  .=  "\nnamespace { Think\Think::addMap(".var_export(self::$_map,true).");";
              $content  .=  "\nL(".var_export(L(),true).");\nC(".var_export(C(),true).');Think\Hook::import('.var_export(Hook::get(),true).');}';
              Storage::put($runtimefile,strip_whitespace('<?php '.$content));
          }else{
            // 调试模式加载系统默认的配置文件
            C(include THINK_PATH.'Conf/debug.php');
            // 读取应用调试配置文件
            if(is_file(CONF_PATH.'debug'.CONF_EXT))
                C(include CONF_PATH.'debug'.CONF_EXT);           
          }
      }

//编译文件
//php_strip_whitespace 就是删除注册可空格之后的php代码了,类似php -w
function compile($filename) {
	$content = php_strip_whitespace ( $filename );
	$content = trim ( substr ( $content, 5 ) );//删除开头的<?php 标记
	// 替换预编译指令
	$content = preg_replace ( '/\/\/\[RUNTIME\](.*?)\/\/\[\/RUNTIME\]/s', '', $content );

    //如果有命名空间的，保留命名空间
	if (0 === strpos ( $content, 'namespace' )) {
		$content = preg_replace ( '/namespace\s(.*?);/', 'namespace \\1{', $content, 1 );
	} else {
		$content = 'namespace {' . $content;
	}

    //如果结尾有 \?\>标记也删除
	if ('?>' == substr ( $content, - 2 ))
		$content = substr ( $content, 0, - 2 );
	return $content . '}';
}