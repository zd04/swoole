<?php
/*
Launch it in a terminal window:

$ php examples/signal.php

In another terminal window find out the pid and send SIGTERM, e.g.:

$ ps aux | grep examp
ruslan    3976  0.2  0.0 139896 11256 pts/1    S+   10:25   0:00 php examples/signal.php
ruslan    3978  0.0  0.0   9572   864 pts/2    S+   10:26   0:00 grep --color=auto examp
$ kill -TERM 3976

At the first terminal window you should catch the following:

Caught signal 15
*/
// SIGHUP = 1
// SIGINT = 2
// SIGQUIT = 3
// SIGILL = 4
// SIGTRAP = 5
// SIGABRT = 6
// SIGIOT = 6
// SIGBUS = 10
// SIGFPE = 8
// SIGUSR1 = 30
// SIGSEGV = 11
// SIGUSR2 = 31
// SIGPIPE = 13
// SIGALRM = 14
// SIGTERM = 15
// SIGSTKFLT not defined 
// SIGCLD not defined 
// SIGCHLD = 20
// SIGCONT = 19
// SIGTSTP = 18
// SIGTTIN = 21
// SIGTTOU = 22
// SIGURG = 16
// SIGXCPU = 24
// SIGXFSZ = 25
// SIGVTALRM = 26
// SIGPROF = 27
// SIGWINCH = 28
// SIGPOLL not defined 
// SIGIO = 23
// SIGPWR not defined 
// SIGSYS = 12
// SIGBABY = 12
// SIG_BLOCK = 1
// SIG_UNBLOCK = 2
// SIG_SETMASK = 3

class MyEventSignal {
    private $base, $ev;

    public function __construct($base) {
        $this->base = $base;
        $this->ev = Event::signal($base, 15, array($this, 'eventSighandler'));
        $this->ev->add();
    }

    public function eventSighandler($no, $c) {
        echo "Caught signal $no\n";
        $this->base->exit();
    }
}

$base = new EventBase();
$c    = new MyEventSignal($base);

$base->loop();