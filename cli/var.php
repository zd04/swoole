<?php
function getVariance($avg, $list, $isSwatch  = FALSE) {
        $arrayCount = count($list);
        if($arrayCount == 1 && $isSwatch == TRUE){
                return FALSE;
        }elseif($arrayCount > 0 ){
                $total_var = 0;
                foreach ($list as $lv)
                        $total_var += pow(($lv - $avg), 2);
                if($arrayCount == 1 && $isSwatch == TRUE)
                        return FALSE;
                return $isSwatch?sqrt($total_var / (count($list) - 1 )):sqrt($total_var / count($list));
        }
        else
                return FALSE;
}

//计算方差的
function var($avg = NULL,$list){

  $listAll = array_values($list);
  $c = count($listAll);
  if($c == 0){
    return FALSE;
  }
  if(is_null($avg)){
    $avgSum = 0;
    foreach($listAll as $one1){
      $avgSum += $one1;
    }
    $avg = $avgSum/$c;
  }
  $sum = 0;
  foreach($listAll as $one){
    $sum += pow(($one - $avg),2);
  }
  return $sum/($c -1);
}