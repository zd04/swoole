<?php 

function settitle($title){
	        // >=php 5.5
        if (function_exists('cli_set_process_title')) {
            @cli_set_process_title($title);
        } // Need proctitle when php<=5.5 .
        elseif (extension_loaded('proctitle') && function_exists('setproctitle')) {
            @setproctitle($title);
        }
}
$pid = pcntl_fork();
if($pid > 0){
	echo "child process=>",getmypid(),PHP_EOL;
	settitle("child");
	sleep(10);
}else if($pid === 0){
	echo "parents process=>",getmypid(),PHP_EOL;
	settitle("parents");
	sleep(10);
}else{
	echo "error",PHP_EOL;
	exit(-1);
}