<?php
$process = new swoole_process('callback_function', true);
$pid = $process->start();

function callback_function(swoole_process $worker)
{
    //在exec 中写入是写到 stdin 标准输入中, 读取是从 stdout 标准输出中读取
    $worker->exec('/usr/bin/php.exe', array(__DIR__.'/stdin_stdout.php'));
}

echo "From Worker: ".$process->read();
$process->write("hello worker\n");
echo "From Worker: ".$process->read();

$ret = swoole_process::wait();
var_dump($ret);

