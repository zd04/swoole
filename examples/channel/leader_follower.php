<?php
use Swoole\Timer;
use Swoole\Process;
use Swoole\Channel;

//创建通道 类似go的chane
$chan = new Channel(1024 * 256);

$worker_num = 4;
$workers = array();

for ($i = 0; $i < $worker_num; $i++)
{
    $process = new Process(function ($worker) use ($chan, $i)
    {
        while (true)
        {
            $data = $chan->pop();//获取数据
            if (empty($data))
            {
                usleep(200000);
                continue;
            }
            echo "worker#$i\t$data\n";
        }
    }, false);
    $process->id = $i;
    $pid = $process->start();
    $workers[$pid] = $process;
}

//定时器
Timer::tick(2000, function () use ($chan)
{
    static $index = 0;
    $chan->push("hello-" . $index++);//写入数据
});